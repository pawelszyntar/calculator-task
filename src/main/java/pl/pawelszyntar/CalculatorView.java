package pl.pawelszyntar;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;
import java.util.logging.Logger;

@Named
@ViewScoped
public class CalculatorView implements Serializable {

    private static final Logger log = Logger.getLogger(CalculatorView.class.getName());

    private String result = "0";
    private double num1;
    private double num2;
    private String operator;
    private double sum;

    /**
     * Updates result by adding to it number (single digit) passed as parameter.
     * @param number
     */
    public void putNumber(int number) {
        if (number == 0 && result.matches("^[0]$")) {
            // do nothing
        } else if (result.matches("^[0]$")) {
            result = "";
            result += number;
        } else {
            result += number;
        }
    }

    /**
     * Clears input text, sets result to empty string and operator to null.
     */
    public void clear() {
        result = "0";
        if (operator != null) {
            operator = null;
        }
        sum = 0;
    }

    /**
     * Assigns operator sign to operator, only if result is a number.
     * Overwrites operator sign if it has been previously assigned.
     *
     * @param operator
     */
    public void operation(String operator) {
        if (result.matches("^[0-9]*$") || result.matches("^[0-9]*\\.[0-9]*$")) {
            if (this.operator == null && !result.isEmpty()) {
                num1 = Double.parseDouble(result);
                this.operator = operator;
                result = "";
            } else if (sum != 0) {
                result = "";
                this.operator = operator;
            } else {
                this.operator = operator;
            }
        }
    }

    /**
     * Assigns result to num2 (if no value is entered, num2 = num1), checks for operator and prints result.
     * If value is type of integer, casts result to integer. In other case, result prints double.
     */
    public void printResult() {
        double res;
        if (!Double.toString(num1).isEmpty() && operator != null && operator.matches("^[+-/*]$") && Double.toString(sum).equals("0.0")) {
            if (result.isEmpty()) {
                num2 = num1; //if no value is entered after operator sign, num1 value is assigned to num2
            } else {
                num2 = Double.parseDouble(result);
            }
            res = calculate();
            if (isInteger(res)) {
                result = Integer.toString((int) res);
            } else {
                result = Double.toString(res);
            }
            sum = res;
        } else if (!Double.toString(sum).isEmpty() && operator != null && operator.matches("^[+-/*]$")) {
            num1 = sum;
            res = calculate();
            if (isInteger(res)) {
                result = Integer.toString((int) res);
            } else {
                result = Double.toString(res);
            }
            sum = res;
        }
    }

    /**
     * Method that returns result of calculation (addition, substraction, multiplication and division respectively
     * to corresponding operator sign. If num2 parameter is 0, division attempt throws exception.
     * @return result value of calculation
     */
    public double calculate() {
        double res = 0;
        switch (operator) {
            case "+":
                res = num1 + num2;
                break;
            case "-":
                res = num1 - num2;
                break;
            case "*":
                res = num1 * num2;
                break;
            case "/":
                if (num2 != 0) {
                    res = num1 / num2;
                    break;
                } else {
                    throw new IllegalArgumentException("Argument divisor equals 0");
                }
        }
        return res;
    }

    /**
     * Checks is number passed as parameter is integer.
     *
     * @param number
     * @return true if number is integer, false if number isn't integer.
     */
    public boolean isInteger(double number) {
        return (number % 1 == 0);
    }

    /**
     * First checks if number contains decimal separator.
     * It it doesn't, updates result by adding comma.
     */
    public void addComma() {
        if (result.isEmpty()) {
            result += "0.";
        } else if (!result.contains(".")) {
            result += ".";
        }
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public double getNum1() {
        return num1;
    }

    public void setNum1(double num1) {
        this.num1 = num1;
    }

    public double getNum2() {
        return num2;
    }

    public void setNum2(double num2) {
        this.num2 = num2;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
}
