package pl.pawelszyntar;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class CalculatorViewTest {

    private CalculatorView calc;

    @Before
    public void init() {
        calc = new CalculatorView();
    }

    @Test
    public void isIntegerParameterIsDouble() {
        double number = 1.5;
        assertFalse(calc.isInteger(number));
    }

    @Test
    public void isIntegerParameterIsInteger() {
        int number = 1;
        assertTrue(calc.isInteger(number));
    }

    @Test
    public void isIntegerParameterIsIntegerButDecimal() {
        double number = 1.000000;
        assertTrue(calc.isInteger(number));
    }

    @Test
    public void addCommaToEmptyString() {
        calc.addComma();
        assertEquals("0.", calc.getResult());
    }

    @Test
    public void addCommaToNumberThatAlreadyContainsComma() {
        calc.setResult("1.22");
        calc.addComma();
        assertEquals("1.22", calc.getResult());
    }

    @Test
    public void addCommaToNumberWithNoComma() {
        calc.setResult("2");
        calc.addComma();
        assertEquals("2.", calc.getResult());
    }

    @Test
    public void putNumberZeroToZero() {
        calc.setResult("0");
        calc.putNumber(0);
        assertEquals("0", calc.getResult());
    }

    @Test
    public void putNumberOtherThanZeroToZero() {
        calc.setResult("0");
        calc.putNumber(12);
        assertEquals("12", calc.getResult());
    }

//    Not sure if should test two separate methods in one test.
    @Test
    public void putCommaAndNumberToEmptyString() {
        calc.addComma();
        calc.putNumber(1);
        assertEquals("0.1", calc.getResult());
    }

    @Test
    public void testAddition() {
        calc.setNum1(1.0);
        calc.setOperator("+");
        calc.setResult("1");
        calc.printResult();
        assertEquals("2", calc.getResult());
    }

    @Test
    public void testSubstraction() {
        calc.setNum1(10);
        calc.setOperator("-");
        calc.setResult("4");
        calc.printResult();
        assertEquals("6", calc.getResult());
    }

    @Test
    public void testMultiplication() {
        calc.setNum1(3);
        calc.setOperator("*");
        calc.setResult("2");
        calc.printResult();
        assertEquals("6", calc.getResult());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDivisionByZero() {
        calc.setNum1(5);
        calc.setOperator("/");
        calc.setResult("0");
        calc.printResult();
    }

    @Test
    public void testDivisionZeroByNumber() {
        calc.setNum1(0);
        calc.setOperator("/");
        calc.setResult("2");
        calc.printResult();
        assertEquals("0", calc.getResult());
    }

    @Test
    public void testDivision() {
        calc.setNum1(6);
        calc.setOperator("/");
        calc.setResult("4");
        calc.printResult();
        assertEquals("1.5", calc.getResult());
    }

    @Test
    public void testPrintResultWhenNoParametersGiven() {
        calc.printResult();
        assertEquals("0", calc.getResult());
    }

    @Test
    public void testPrintResultWhenNoOperatorIsNull() {
        calc.setOperator(null);
        assertEquals("0", calc.getResult());
    }

    @Test
    public void testChecksForAssigningValueToNum1() {
        calc.setResult("5");
        calc.operation("+");
        assertThat(calc.getNum1(), is(5.0));
        assertThat(calc.getOperator(), is("+"));
    }

    @Test
    public void testForMultipleOperationMethodInvocation() {
        calc.setResult("3");
        calc.operation("-");
        calc.operation("*");
        calc.operation("+");
        calc.setResult("5");
        calc.printResult();
        assertEquals("8", calc.getResult());
    }

    @Test
    public void testNumberEnteredIsOutOfIntegerBoundaries() {
        calc.setResult("12451648982585426");
        calc.operation("+");
        calc.setResult("456784153545978413");
        calc.printResult();
        assertEquals(Integer.toString(Integer.MAX_VALUE), calc.getResult());
    }


}